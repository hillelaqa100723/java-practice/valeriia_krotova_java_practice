package hw4;

public class ArrayDiag {
    public static void main(String[] args) {

        int[][] array =
                {
                        {1, 2, 3},
                        {4, 5, 6},
                        {7, 8, 9}
                };

        System.out.print("Головна діагональ: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i][i] + ", ");
        }
        System.out.println();

        System.out.print("Побічна діагональ: ");

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[array.length - i - 1][i] + ", ");
        }
        System.out.println();
    }
}
