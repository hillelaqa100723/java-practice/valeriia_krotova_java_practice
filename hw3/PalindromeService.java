package hw3;

public class PalindromeService {
    public Boolean isPalindrome(String str) {
        String noTabString = str.replaceAll(" ", "");

        boolean isPalindrome = true;
        for (int i = 0; i < (noTabString.length() / 2); i++) {

            if (noTabString.charAt(i) != noTabString.charAt(noTabString.length() - 1 - i)) {
                isPalindrome = false;
                break;
            }

        }

        return isPalindrome;
    }
}
