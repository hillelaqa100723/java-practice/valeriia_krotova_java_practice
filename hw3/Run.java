package hw3;

import hw2.PalindromeService;

import java.util.Scanner;

public class Run {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter array of numbers: ");
        String text = scanner.nextLine();

        PalindromeService messageService = new PalindromeService();

        Boolean result = messageService.isPalindrome(text);

        if (result) {
            System.out.println("It's palindrome");
        } else {
            System.out.println("It's not palindrome");
        }
    }
}
