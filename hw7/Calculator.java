package hw7;

import java.util.List;

public class Calculator {

    private static OPERATION getOperation(String operatorStr) {
        for (OPERATION operation : OPERATION.values()) {
            if (operation.getValue().equals(operatorStr)) {
                return operation;
            }
        }
        return null;
    }

    public static Double calculate(List<String> data) {
        if (data == null || data.isEmpty()) {
            System.out.println("Помилка: вхідні дані відсутні.");
            return null;
        }

        double x = Double.parseDouble(data.get(0));
        double y = Double.parseDouble(data.get(2));

        String operatorStr = data.get(1);

        Double result = null;
        OPERATION operation = getOperation(operatorStr);

        if (operation == null) {
            System.out.println("Помилка: некоректний оператор.");
            return null;
        }

        switch (operation) {
            case PLUS -> result = x + y;
            case MINUS -> result = x - y;
            case DIVIDE -> {
                if (y == 0) {
                    System.out.println("Помилка: ділення на ноль.");
                    return null;
                }
                result = x / y;
            }
            case MULTIPLY -> result = x * y;
        }


        return result;
    }

    public static String prepareResultString(List<String> data, Double result) {
        return data.get(0) + " " +
                getOperation(data.get(1)) + " " +
                data.get(2) + " " +
                "=" + " " +
                (result != null ? result : "null");
    }

    public static void main(String[] args) {
        List<String> inputData = List.of("5", "PLUS", "5");
        Double result = calculate(inputData);
        String resultString = prepareResultString(inputData, result);
        System.out.println(resultString);
    }

    enum OPERATION {
        PLUS("PLUS", "+"),
        MINUS("MINUS", "-"),
        DIVIDE("DIVIDE", "/"),
        MULTIPLY("MULTIPLY", "*");

        private final String value;
        private final String symbol;

        OPERATION(String value, String symbol) {
            this.value = value;
            this.symbol = symbol;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return symbol;
        }
    }
}

