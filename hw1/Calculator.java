package hw1;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        System.out.print("Enter first value: ");
        int a = scanner.nextInt();

        System.out.print("Enter second value: ");
        int b = scanner.nextInt();

        prinResult("Sum: ", plus(a, b));
        prinResult("Odd: ", minus(a, b));
        prinResult("Mult: ", mult(a, b));
        prinResult("Div: ", div(a, b));
        prinResult("Rem: ", rem(a, b));


        scanner.close();


    }

    public static void prinResult(String comment, int value) {
        System.out.println(comment + value);
    }

    /**
     * Adding one inter to another
     *
     * @param a first integer
     * @param b second integer
     * @return result as integer
     */
    public static int plus(int a, int b) {
        return a + b;
    }

    public static int plus(double a, int b) {
        return (int) a + b;
    }

    public static int minus(int a, int b) {
        return a - b;
    }

    public static int mult(int a, int b) {
        return a * b;
    }

    public static int div(int a, int b) {
        if (b != 0) {
            return a / b;
        } else {
            System.out.println("You cannot divide on 0!");
            return -10000;
        }
    }

    public static int rem(int a, int b) {
        if (b != 0) {
            return a % b;
        } else {
            System.out.println("You cannot divide on 0!");
            return -10000;
        }
    }

}



