package hw2;

import java.util.Scanner;

public class Run {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter text: ");
        String text = scanner.nextLine();

        PalindromeService messageService = new PalindromeService();

//        Boolean result = messageService.isPalindrome("rty ytR");
//        Boolean result = messageService.isPalindrome("кит на морі романтик");
        Boolean result = messageService.isPalindrome(text);

        if (result) {
            System.out.println("It's palindrome");
        } else {
            System.out.println("It's not palindrome");
        }
    }
}
