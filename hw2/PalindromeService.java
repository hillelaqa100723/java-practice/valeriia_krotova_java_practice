package hw2;

public class PalindromeService {
    public Boolean isPalindrome(String str) {
        String noTabString = str.replaceAll(" ", "");
        String lowCaseString = noTabString.toLowerCase();

        boolean isPalindrome = true;
        for (int i = 0; i < (lowCaseString.length() / 2); i++) {

            if (lowCaseString.charAt(i) != lowCaseString.charAt(lowCaseString.length() - 1 - i)) {
                isPalindrome = false;
                break;
            }

        }

        return isPalindrome;
    }
}
