package hw9;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WorkFile {

    public static Map<Integer, String> getDataFromFile(File dataFile) throws IOException {

        Map<Integer, String> map = new HashMap<>();

        List<String> strings = FileUtils.readLines(dataFile, Charset.defaultCharset());

        for (String str : strings) {
            String[] splitStr = str.split(",");
            map.put(Integer.valueOf(splitStr[0]), splitStr[1]);
        }

        return map;
    }

    public static String getDataById(Map<Integer, String> mapData, Integer id) {
        return mapData.get(id);
    }

    public static int getNumberOfOccurrences(Map<Integer, String> mapData, String lastName) {
        int count = 0;

        List<String> list = new ArrayList<>(mapData.values());
        for (String fullName : list) {
            if (fullName.split(" ")[0].equals(lastName)) count++;
        }

        return count;
    }

    public static void main(String[] args) {

        File file = new File("hw9/data.txt");
        Map<Integer, String> dataMap;
        try {
            dataMap = getDataFromFile(file);
            System.out.println(getDataById(dataMap, 8562));
            System.out.println(getNumberOfOccurrences(dataMap, "Ivanov"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
