package hw5;

public class ArraySum {

    public static int[] sumsInRows(int[][] source) {
        int[] sums = new int[source.length];
        for (int i = 0; i < source.length; i++) {
            for (int j = 0; j < source[i].length; j++) {
                sums[i] += source[i][j];
            }
        }
        return sums;
    }

    public static void main(String[] args) {

        int[][] array =
                {
                        {1, 2, 3},
                        {4, 5, 6},
                        {7, 8, 9}
                };

        int[] arraySum = sumsInRows(array);

        System.out.print("Результат: ");
        for (int i : arraySum) {
            System.out.print(i + ", ");
        }
        System.out.println();
    }
}
